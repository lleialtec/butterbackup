# butterbackup

A backup solution. A server at your office behind your firewall that pulls data from public servers and saves snapshots.

# On your backup server
Create a partition from free space

```
cfdisk /dev/sda
apt-get install parted
partprobe
apt-get install btrfs-tools
mkfs.btrfs /dev/sdaX
mkdir /backups
mount /dev/sdaX /backups
```


## Mount the partition on boot
Find the UUID of the new partition
`blkid`

Edit /etc/fstab
`UUID=7ef18851-xxxx-yyyy-zzzz-53f7b88b6119 /backups          btrfs   defaults      0  0`


## Setup butterbackup
```
apt-get install git
cd /opt
git clone https://gitlab.com/lleialtec/butterbackup.git
```


Instalar python3 en un entorno virtual
```
apt-get install virtualenv
cd /opt/butterbackup
virtualenv venv --python=python3
```



# Setup users

butterbackup runs on the backserver as root. root connects to the public server as user keeper.

We create a user on the public server and copy root@backup_server public key to keeper@public_server authorized_keys

On the public_server

## Create dummy user on public server

```
adduser --disabled-password keeper
mkdir /home/keeper/.ssh
touch /home/keeper/.ssh/authorized_keys
```



## Create root@backup_server ssh keys on the backup_server

`ssh-keygen -t ecdsa -b 521`

And add the new public key to keeper@public_server /home/keeper/.ssh/authorized_keys

Test to see if you can open a session on the public_server

`ssh keeper@public_server`

## Permissions
Perhaps keeper@public_server does not have permission to read everything you want to backup.

Let's say you want to backup /var/www on the public server. You can add the keeper to the www-data group

# Create a config file
```
cd /opt/butterbackup/config
cp example.com my_server_domain_name.com
```


Edit the new config file.

# Make the first copy
You can run butterbackup manually

`/opt/butterbackup/venv/bin/python3 /opt/butterbackup/butterbackup.py my_server_domain_name.com`

# Add an entry to root's crontab

`crontab -e`

You cronjobs might look like this. Where each server has a corresponding `/opt/butterbackup/config` file

```
0  2 * * * /opt/butterbackup/venv/bin/python3 /opt/butterbackup/butterbackup.py server_1.com
0  3 * * * /opt/butterbackup/venv/bin/python3 /opt/butterbackup/butterbackup.py server_2.com
15 3 * * * /opt/butterbackup/venv/bin/python3 /opt/butterbackup/butterbackup.py server_3.com
45 3 * * 1,4 /opt/butterbackup/venv/bin/python3 /opt/butterbackup/butterbackup.py server_4.com
```

# Inspect backups

`cd /backups/my_server_domain_name.com`
